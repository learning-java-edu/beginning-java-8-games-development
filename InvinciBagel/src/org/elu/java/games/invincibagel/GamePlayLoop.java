package org.elu.java.games.invincibagel;

import javafx.animation.AnimationTimer;

public class GamePlayLoop extends AnimationTimer {
    private InvinciBagel invinciBagel;

    GamePlayLoop(InvinciBagel iBagel) {
        this.invinciBagel = iBagel;
    }

    @Override
    public void handle(long now) {
        invinciBagel.getBagel().update();
        invinciBagel.getEnemy().update();
    }

    @Override
    public void start() {
        super.start();
    }

    @Override
    public void stop() {
        super.stop();
    }
}
