package org.elu.java.games.invincibagel;

import javafx.scene.image.Image;

public class PropB extends Actor {
    PropB(String SVGdata, double xLocation, double yLocation, Image... spriteCels) {
        super(SVGdata, xLocation, yLocation, spriteCels);
        setFlipH(true);
        getSpriteFrame().setScaleX(-1);
        setFlipV(true);
        getSpriteFrame().setScaleY(-1);
        getSpriteFrame().setTranslateX(xLocation);
        getSpriteFrame().setTranslateY(yLocation);
    }

    @Override
    public void update() {
        //
    }
}
