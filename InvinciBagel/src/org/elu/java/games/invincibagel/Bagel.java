package org.elu.java.games.invincibagel;

import javafx.scene.image.Image;
import javafx.scene.shape.SVGPath;
import javafx.scene.shape.Shape;

import static org.elu.java.games.invincibagel.InvinciBagel.HEIGHT;
import static org.elu.java.games.invincibagel.InvinciBagel.WIDTH;

public class Bagel extends Hero {
    private InvinciBagel invinciBagel;
    private static final double SPRITE_PIXELS_X = 81;
    private static final double SPRITE_PIXELS_Y = 81;
    private static final double rightBoundary = WIDTH - SPRITE_PIXELS_X;
    private static final double leftBoundary = 0;
    private static final double bottomBoundary = HEIGHT - SPRITE_PIXELS_Y;
    private static final double topBoundary = 0;
    private boolean animator = false;
    private int framecounter = 0;
    private int runningspeed = 6;

    Bagel(InvinciBagel iBagel, String SVGdata, double xLocation, double yLocation, Image... spriteCels) {
        super(SVGdata, xLocation, yLocation, spriteCels);
        invinciBagel = iBagel;
    }

    @Override
    public void update() {
        setXYLocation();
        setBoundaries();
        setImageState();
        moveInvinciBagel(getiX(), getiY());
//        playAudioClip();
        checkCollision();
    }

    private void setXYLocation() {
        if (invinciBagel.isRight()) {
            moveRight();
        }
        if (invinciBagel.isLeft()) {
            moveLeft();
        }
        if (invinciBagel.isDown()) {
            moveDown();
        }
        if (invinciBagel.isUp()) {
            moveUp();
        }
    }

    private void setBoundaries() {
        if (getiX() >= rightBoundary) {
            setiX(rightBoundary);
        }
        if (getiX() <= leftBoundary) {
            setiX(leftBoundary);
        }
        if (getiY() >= bottomBoundary) {
            setiY(bottomBoundary);
        }
        if (getiY() <= topBoundary) {
            setiY(topBoundary);
        }
    }

    private void setImageState() {
        if (!invinciBagel.isRight() && !invinciBagel.isLeft() &&
                !invinciBagel.isDown() && !invinciBagel.isUp()) {
            getSpriteFrame().setImage(getImageStates().get(0));
            animator = false;
            framecounter = 0;
        }
        if (invinciBagel.isRight()) {
            getSpriteFrame().setScaleX(1);
            setFlipH(false);
            animateRun();
        }
        if (invinciBagel.isLeft()) {
            getSpriteFrame().setScaleX(-1);
            setFlipH(true);
            animateRun();
        }
        if (invinciBagel.isDown()) {
            getSpriteFrame().setImage(getImageStates().get(6));
        }
        if (invinciBagel.isUp()) {
            getSpriteFrame().setImage(getImageStates().get(4));
        }
        if (invinciBagel.iswKey()) {
            getSpriteFrame().setImage(getImageStates().get(5));
        }
        if (invinciBagel.issKey()) {
            getSpriteFrame().setImage(getImageStates().get(8));
        }
    }

    private void animateRun() {
        if (!invinciBagel.isDown() && !invinciBagel.isUp()) {
            if (framecounter < runningspeed) {
                framecounter += 1;
                return;
            }
            if (animator) {
                getSpriteFrame().setImage(getImageStates().get(2));
                animator = false;
            } else {
                getSpriteFrame().setImage(getImageStates().get(1));
                animator = true;
            }
            framecounter = 0;
        }
    }

    private void moveInvinciBagel(double x, double y) {
        getSpriteFrame().setTranslateX(x);
        getSpriteFrame().setTranslateY(y);
    }

    private void playAudioClip() {
        if (invinciBagel.isLeft()) {
            invinciBagel.playiSound0();
        }
        if (invinciBagel.isRight()) {
            invinciBagel.playiSound1();
        }
        if (invinciBagel.isUp()) {
            invinciBagel.playiSound2();
        }
        if (invinciBagel.isDown()) {
            invinciBagel.playiSound3();
        }
        if (invinciBagel.iswKey()) {
            invinciBagel.playiSound4();
        }
        if (invinciBagel.issKey()) {
            invinciBagel.playiSound5();
        }
    }

    private void checkCollision() {
        for (Actor object : invinciBagel.getCastDirector().getCurrentCast()) {
            if (collide(object)) {
                invinciBagel.getCastDirector().addToRemovedActors(object);
                invinciBagel.getRoot().getChildren().remove(object.getSpriteFrame());
                invinciBagel.getCastDirector().resetRemovedActors();
                scoringEngine(object);
            }
        }
    }

    private void scoringEngine(Actor object) {
        if (object instanceof Prop) {
            invinciBagel.addGameScore(-1);
            invinciBagel.playiSound0();
        } else if (object instanceof PropV) {
            invinciBagel.addGameScore(-2);
            invinciBagel.playiSound1();
        } else if (object instanceof PropH) {
            invinciBagel.addGameScore(-1);
            invinciBagel.playiSound2();
        } else if (object instanceof PropB) {
            invinciBagel.addGameScore(-2);
            invinciBagel.playiSound3();
        } else if (object instanceof Treasure) {
            invinciBagel.addGameScore(5);
            invinciBagel.playiSound4();
        } else if (object.equals(invinciBagel.getBullet())) {
            invinciBagel.addGameScore(-5);
            invinciBagel.playiSound5();
        } else if (object.equals(invinciBagel.getCheese())) {
            invinciBagel.addGameScore(5);
            invinciBagel.playiSound0();
        } else if (object.equals(invinciBagel.getEnemy())) {
            invinciBagel.addGameScore(10);
            invinciBagel.playiSound0();
        }
        invinciBagel.updateScoreText();
    }

    @Override
    public boolean collide(Actor actor) {
        if (invinciBagel.getBagel().getSpriteFrame().getBoundsInParent().intersects(
                actor.getSpriteFrame().getBoundsInParent())) {
            Shape intersection = SVGPath.intersect(invinciBagel.getBagel().getSpriteBound(), actor.getSpriteBound());
            return intersection.getBoundsInLocal().getWidth() != -1;
        }
        return false;
    }
}
