package org.elu.java.games.invincibagel;

import javafx.scene.image.Image;

public class PropV extends Actor {
    PropV(String SVGdata, double xLocation, double yLocation, Image... spriteCels) {
        super(SVGdata, xLocation, yLocation, spriteCels);
        setFlipV(true);
        getSpriteFrame().setScaleY(-1);
        getSpriteFrame().setTranslateX(xLocation);
        getSpriteFrame().setTranslateY(yLocation);
    }

    @Override
    public void update() {
        //
    }
}
