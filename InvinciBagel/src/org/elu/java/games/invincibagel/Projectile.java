package org.elu.java.games.invincibagel;

import javafx.scene.image.Image;

public class Projectile extends Actor {
    Projectile(String SVGdata, double xLocation, double yLocation, Image... spriteCels) {
        super(SVGdata, xLocation, yLocation, spriteCels);
        getSpriteFrame().setTranslateX(xLocation);
        getSpriteFrame().setTranslateY(yLocation);
        setFixed(false);
        setHasValu(true);
        setBonus(true);
    }

    @Override
    public void update() {
    }
}
