package org.elu.java.games.invincibagel;

import javafx.scene.image.Image;

public class Prop extends Actor {
    Prop(String SVGdata, double xLocation, double yLocation, Image... spriteCels) {
        super(SVGdata, xLocation, yLocation, spriteCels);
        getSpriteFrame().setTranslateX(xLocation);
        getSpriteFrame().setTranslateY(yLocation);
    }

    @Override
    public void update() {
        //
    }
}
