package org.elu.java.games.invincibagel;

import javafx.scene.image.Image;

import java.util.Random;

public class Enemy extends Actor {
    private static final Random randomNum = new Random();
    private int attackCounter = 0;
    private int attackFrequency = 250;
    private int attackBoundary = 300;
    private boolean takeSides = false;
    private boolean onScreen = false;
    private boolean callAttack = false;
    private boolean shootBullet = false;
    private int spriteMoveR, spriteMoveL, destination, randomLocation, bulletRange, bulletOffset;
    private double randomOffset;
    private double bulletGravity = 0.2;
    private double cheeseGravity = 0.1;
    private int pauseCounter = 0;
    private boolean launchIt = false;
    private boolean bulletType = false;
    private int iBagelLocation;
    private InvinciBagel invinciBagel;

    Enemy(InvinciBagel iBagel, String SVGdata, double xLocation, double yLocation, Image... spriteCels) {
        super(SVGdata, xLocation, yLocation, spriteCels);
        invinciBagel = iBagel;
        getSpriteFrame().setTranslateX(xLocation);
        getSpriteFrame().setTranslateY(yLocation);
        setAlive(true);
        setHasValu(true);
        setBonus(true);
    }

    @Override
    public void update() {
        if (!callAttack) {
            if (attackCounter >= attackFrequency) {
                attackCounter = 0;
                spriteMoveR = 700;
                spriteMoveL = -70;
                randomLocation = randomNum.nextInt(attackBoundary);
                iBagelLocation = (int) invinciBagel.getBagel().getiY();
                bulletType = randomNum.nextBoolean();
                if (bulletType) {
                    getSpriteFrame().setTranslateY(randomLocation);
                    randomOffset = randomLocation + 5;
                } else {
                    getSpriteFrame().setTranslateY(iBagelLocation);
                    randomOffset = iBagelLocation + 5;
                }
                takeSides = randomNum.nextBoolean();
                callAttack = true;
            } else {
                attackCounter += 1;
            }
        } else {
            initiateAttack();
        }
        if (shootBullet) {
            shootProjectile();
            if (pauseCounter >= 60) {
                launchIt = true;
                pauseCounter = 0;
            } else {
                pauseCounter++;
            }
        }
    }

    private void initiateAttack() {
        if (!takeSides) {
            getSpriteFrame().setScaleX(1);
            setFlipH(false);
            if (!onScreen) {
                destination = 500;
                if (spriteMoveR >= destination) {
                    spriteMoveR -= 2;
                    getSpriteFrame().setTranslateX(spriteMoveR);
                } else {
                    bulletOffset = 480;
                    shootBullet = true;
                    onScreen = true;
                }
            }
            if (onScreen && launchIt) {
                destination = 700;
                if (spriteMoveR <= destination) {
                    spriteMoveR += 1;
                    getSpriteFrame().setTranslateX(spriteMoveR);
                } else {
                    resetEnemy();
                }
            }
        }
        if (takeSides) {
            getSpriteFrame().setScaleX(-1);
            setFlipH(true);
            if (!onScreen) {
                destination = 100;
                if (spriteMoveL <= destination) {
                    spriteMoveL += 2;
                    getSpriteFrame().setTranslateX(spriteMoveL);
                } else {
                    bulletOffset = 120;
                    shootBullet = true;
                    onScreen = true;
                }
            }
            if (onScreen && launchIt) {
                destination = -70;
                if (spriteMoveL >= destination) {
                    spriteMoveL -= 1;
                    getSpriteFrame().setTranslateX(spriteMoveL);
                } else {
                    resetEnemy();
                }
            }
        }
    }

    private void resetEnemy() {
        onScreen = false;
        callAttack = false;
        launchIt = false;
        loadBullet();
        loadCheese();
        loadEnemy();
        attackFrequency = 60 + randomNum.nextInt(480);
    }

    private void shootProjectile() {
        if (!bulletType && !takeSides) {
            invinciBagel.getBullet().getSpriteFrame().setTranslateY(randomOffset);
            invinciBagel.getBullet().getSpriteFrame().setScaleX(-0.5);
            invinciBagel.getBullet().getSpriteFrame().setScaleY(0.5);
            bulletRange = -50;
            if (bulletOffset >= bulletRange) {
                bulletOffset -= 6;
                invinciBagel.getBullet().getSpriteFrame().setTranslateX(bulletOffset);
                randomOffset = randomOffset + bulletGravity;
            } else {
                shootBullet = false;
            }
        }
        if (!bulletType && takeSides) {
            invinciBagel.getBullet().getSpriteFrame().setTranslateY(randomOffset);
            invinciBagel.getBullet().getSpriteFrame().setScaleX(0.5);
            invinciBagel.getBullet().getSpriteFrame().setScaleY(0.5);
            bulletRange = 624;
            if (bulletOffset <= bulletRange) {
                bulletOffset += 6;
                invinciBagel.getBullet().getSpriteFrame().setTranslateX(bulletOffset);
                randomOffset = randomOffset + bulletGravity;
            } else {
                shootBullet = false;
            }
        }
        if (bulletType && !takeSides) {
            invinciBagel.getCheese().getSpriteFrame().setTranslateY(randomOffset);
            invinciBagel.getCheese().getSpriteFrame().setScaleX(-0.5);
            invinciBagel.getCheese().getSpriteFrame().setScaleY(0.5);
            bulletRange = -50;
            if (bulletOffset >= bulletRange) {
                bulletOffset -= 4;
                invinciBagel.getCheese().getSpriteFrame().setTranslateX(bulletOffset);
                randomOffset = randomOffset + cheeseGravity;
            } else {
                shootBullet = false;
            }
        }
        if (bulletType && takeSides) {
            invinciBagel.getCheese().getSpriteFrame().setTranslateY(randomOffset);
            invinciBagel.getCheese().getSpriteFrame().setScaleX(0.5);
            invinciBagel.getCheese().getSpriteFrame().setScaleY(0.5);
            bulletRange = 630;
            if (bulletOffset <= bulletRange) {
                bulletOffset += 4;
                invinciBagel.getCheese().getSpriteFrame().setTranslateX(bulletOffset);
                randomOffset = randomOffset + cheeseGravity;
            } else {
                shootBullet = false;
            }
        }
    }

    private void loadBullet() {
        for (Actor object : invinciBagel.getCastDirector().getCurrentCast()) {
            if (object.equals(invinciBagel.getBullet())) {
                return;
            }
        }
        invinciBagel.getCastDirector().addCurrentCast(invinciBagel.getBullet());
        invinciBagel.getRoot().getChildren().add(invinciBagel.getBullet().getSpriteFrame());
    }

    private void loadCheese() {
        for (Actor object : invinciBagel.getCastDirector().getCurrentCast()) {
            if (object.equals(invinciBagel.getCheese())) {
                return;
            }
        }
        invinciBagel.getCastDirector().addCurrentCast(invinciBagel.getCheese());
        invinciBagel.getRoot().getChildren().add(invinciBagel.getCheese().getSpriteFrame());
    }

    private void loadEnemy() {
        for (Actor object : invinciBagel.getCastDirector().getCurrentCast()) {
            if (object.equals(invinciBagel.getEnemy())) {
                return;
            }
        }
        invinciBagel.getCastDirector().addCurrentCast(invinciBagel.getEnemy());
        invinciBagel.getRoot().getChildren().add(invinciBagel.getEnemy().getSpriteFrame());
    }
}
