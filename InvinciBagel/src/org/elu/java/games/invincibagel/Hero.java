package org.elu.java.games.invincibagel;

import javafx.scene.image.Image;

public abstract class Hero extends Actor {
    private double vX;
    private double vY;
    private double lifeSpan;
    private double damage;
    private double offsetX;
    private double offsetY;
    private double boundScale;
    private double boundRot;
    private double friction;
    private double gravity;
    private double bounce;

    Hero(String SVGdata, double xLocation, double yLocation, Image... spriteCels) {
        super(SVGdata, xLocation, yLocation, spriteCels);
        lifeSpan = 1000;
        vX = vY = 1;
    }

    void moveRight() {
        this.updateX(vX);
    }

    void moveLeft() {
        this.updateX(-vX);
    }

    void moveDown() {
        this.updateY(vY);
    }

    void moveUp() {
        this.updateY(-vY);
    }

    public boolean collide(Actor object) {
        return false;
    }

    public double getvX() {
        return vX;
    }

    public void setvX(double vX) {
        this.vX = vX;
    }

    public double getvY() {
        return vY;
    }

    public void setvY(double vY) {
        this.vY = vY;
    }

    public double getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(double lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getOffsetX() {
        return offsetX;
    }

    public void setOffsetX(double offsetX) {
        this.offsetX = offsetX;
    }

    public double getOffsetY() {
        return offsetY;
    }

    public void setOffsetY(double offsetY) {
        this.offsetY = offsetY;
    }

    public double getBoundScale() {
        return boundScale;
    }

    public void setBoundScale(double boundScale) {
        this.boundScale = boundScale;
    }

    public double getBoundRot() {
        return boundRot;
    }

    public void setBoundRot(double boundRot) {
        this.boundRot = boundRot;
    }

    public double getFriction() {
        return friction;
    }

    public void setFriction(double friction) {
        this.friction = friction;
    }

    public double getGravity() {
        return gravity;
    }

    public void setGravity(double gravity) {
        this.gravity = gravity;
    }

    public double getBounce() {
        return bounce;
    }

    public void setBounce(double bounce) {
        this.bounce = bounce;
    }
}
